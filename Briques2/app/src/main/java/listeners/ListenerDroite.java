package listeners;

import android.view.View;

import mihetsika.Plateau;

/**
 * Created by ITU on 05/04/2018.
 */
public class ListenerDroite implements View.OnClickListener {
    private Plateau plate;

    public ListenerDroite(Plateau p){
        plate=p;
    }
    @Override
    public void onClick(View v) {
        plate.deplacer(true);
    }
}
