package listeners;

import android.view.View;

import mihetsika.Plateau;

/**
 * Created by ITU on 05/04/2018.
 */
public class ListenerGauche implements View.OnClickListener{
    private Plateau plate;
    public ListenerGauche(Plateau p){
        plate=p;
    }

    @Override
    public void onClick(View v) {
        plate.deplacer(false);
    }
}
