package listeners;

import android.view.View;
import android.widget.Button;

import mihetsika.Balle;
import threads.DepBalle;

/**
 * Created by ITU on 05/04/2018.
 */

public class ListenerBalle implements View.OnClickListener {
    private DepBalle dball;
    public ListenerBalle(DepBalle db){
        dball=db;
    }
    @Override
    public void onClick(View v) {
        if(dball.isAlive()==false) {
            dball.start();
        }
    }

}
