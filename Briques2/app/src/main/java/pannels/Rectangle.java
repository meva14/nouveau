package pannels;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import mihetsika.Balle;
import mihetsika.Brique;
import mihetsika.Plateau;
import score.Score;

/**
 * Created by ITU on 04/04/2018.
 */
public class Rectangle extends View {
    private int x;
    private int y;
    private int width;
    private int height;
    //private Balle ball;
    private Plateau plate;
    private Button gauche;
    private Button droite;
    private Button start;
    private Paint paint;
    private ArrayList<Brique> briques=new ArrayList();
    private ArrayList<Balle> listeBall=new ArrayList();
    private Score score=new Score();
    private int nb=0;

    public ArrayList<Balle> getListeBall(){
        return listeBall;
    }
    public Score getScore(){return score;}
    public void setBriques(int li,int co){
        briques=new ArrayList();
    }

    public ArrayList<Brique> getBriq(){
        return briques;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public Rectangle(Context context) {
        super(context);
        DisplayMetrics metrics=context.getResources().getDisplayMetrics();
        setMinimumWidth(metrics.widthPixels);
        setMinimumHeight(metrics.heightPixels);
        setMeasuredDimension(metrics.widthPixels,metrics.heightPixels);

        plate=new Plateau(this);
        //ball=new Balle(this);
        paint=new Paint();
        int xb=1;int yb=1;
        int pt=1;
        Balle ba=new Balle(this);
        listeBall.add(ba);
        listeBall.get(0).setIndBalle(1);
        for(int i=0;i<18;i++){
            Brique b=new Brique(this,xb,yb,pt);

            briques.add(b);
            xb+=b.getWidth()+1;
            if((i+1)%6==0){
                xb=1;
                yb+=b.getHeight()+1;
                pt++;
            }
        }
        score.setSizeCanvas(getMinimumWidth()-190,getMinimumHeight()-250);
    }
    public void onDraw(Canvas canvas){
        super.onDraw(canvas);
        plate.afficher(canvas,paint);
        //ball.afficher(canvas,paint);
        for(int j=0;j<listeBall.size();j++) {
            listeBall.get(j).afficher(canvas, paint);
        }
        score.afficher(canvas,paint);
        for(int i=0;i<briques.size();i++){
            Brique b=(Brique)briques.get(i);
            b.afficher(canvas,paint);
        }
    }
    public void ajouterBalle(){
        Balle b=new Balle(this);
        listeBall.add(b);
        int i=listeBall.indexOf(b);
        listeBall.get(i).setIndBalle(i+1);
    }
    public Paint getPaint(){
        return paint;
    }

    public void addPlateau(Plateau p){
        plate=p;
    }

    /*public void addBalle(Balle b){
        ball=b;
    }*/
    public Plateau getPlateau(){ return plate; }

    /*public Balle getBalle(){
        return ball;
    }*/
    public Balle getBalle(int ind){
        return listeBall.get(ind);
    }
    public ArrayList<Balle> getBalle(){
        return listeBall;
    }
    /*public Balle getBalle(){
        return ball;
    }*/
}
