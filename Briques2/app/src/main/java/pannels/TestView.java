package pannels;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.example.itu.briques2.R;

/**
 * Created by ITU on 05/04/2018.
 */
public class TestView extends View {
    private Paint paint;
    private Button gauche;
    private Button droite;

    public TestView(Context context) {
        super(context);
        paint=new Paint();
    }


    protected void onDraw(Canvas canvas){
        super.onDraw(canvas);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.parseColor("#CD5C5C"));

        int width=getWidth();
        int height=getHeight();
        float left=(width/2)-150,top=getHeight()-50,right=left+300,bottom=top+10;
        canvas.drawRect(left,top,right,bottom, paint);

        paint.setColor(Color.parseColor("#FFFFFF"));
        canvas.drawCircle((width/2)-7,top-15,15,paint);

    }

}
