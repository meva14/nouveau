package mihetsika;

import android.annotation.TargetApi;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.util.Log;

import pannels.Rectangle;

/**
 * Created by ITU on 05/04/2018.
 */
public class Plateau extends Deplacable {
    int nb;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public Plateau(Rectangle cont) {
        super(cont);
        int width=conteneur.getMinimumWidth();
        int height=conteneur.getMinimumHeight();

        setWidth(150);
        setHeight(10);
        x=(width/2)-(getWidth()/2);
        y=height-(height/3);
        dx=20;
    }
    public void afficher(Canvas canvas,Paint paint){
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.parseColor("#FFAD036B"));
        float left=x ,top=y,right=left+this.width,bottom=top+this.height;
        canvas.drawRect(left,top,right,bottom,paint);
    }
    public void setX(int coordX){
        x=coordX;
        if(x<0) x=0;
        if(x>conteneur.getWidth()-getWidth()) x=conteneur.getWidth()-getWidth();
    }
    public void setY(int coordY){
        y=coordY;
        if(y>conteneur.getHeight()-getWidth()){
            y=conteneur.getHeight()-getWidth();
        }
    }

    @Override
    public void deplacer(){
        Balle b=conteneur.getBalle(nb);//<--here
        setX(x+getDx());
        if(b.getMikasoka()){
            b.setX(b.getX()+getDx());
        }
        conteneur.postInvalidate();
    }

    public void deplacer(boolean test){
        if(test==true){ //droite
            if(getDx()<0){setDx(-getDx());}
            deplacer();
        }else{ //gauche
            if(getDx()>0){setDx(-getDx());}
            deplacer();
        }

    }


}
