package mihetsika;

import pannels.Rectangle;

/**
 * Created by ITU on 04/04/2018.
 */
public abstract class Deplacable {
    int x;
    int y;
    int width;
    int height;
    int dx;
    int dy;
    Rectangle conteneur;

   /* public void collisionPlateau(Plateau p){
        if(testSisiny(p)==true){
            setDerivee(-dx,-dy);
        }
        if(x>=p.getX() && x<=p.getWidth()+p.getX()){
            if((p.getY()-y)==(height/2)){
                setDy(-dy);
            }
        }
    }

    public void collisionLB(ArrayList<Brique> lb){
        for(int i=0;i<lb.size();i++){
            if(testerCollision((Brique)lb.get(i))==true){
                Brique br=(Brique)lb.get(i);
                conteneur.getScore().setPoint(br.getPoint());
                lb.remove(i);
                break;
            }
        }
    }
    public boolean testerCollision(Brique b){
        boolean test=false;
        if(testSisiny(b)==true){
            setDerivee(-dx,-dy);
            test=true;
        }
        if(x>=b.getX() && x<=b.getWidth()+b.getX()){
            if(((b.getY() - y) >= (height / 2)) || (y-b.getY()<=(b.getHeight()+(height/2)))){
                setDy(-dy);
                test=true;
            }
        }
        if(y>=b.getY() && y<=b.getY()+b.getHeight() && ((b.getX()-x)<=(width/2) || ((x-(b.getX()+b.getWidth()))<=(width/2)))){
            //if((b.getX()-x)<=(width/2) || ((x-(b.getX()+b.getWidth()))<=(width/2)))
            setDx(-dx);
            //test=true;
        }
        return test;
    }*/

    public void setDerivee(int derx,int dery){
        setDx(derx);
        setDy(dery);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getDx() {
        return dx;
    }

    public int getDy() {
        return dy;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void setX(int x){
        this.x=x;
    }

    public void setY(int y){
        this.y=y;
    }

    public void setDx(int dx) {
        this.dx = dx;
    }

    public void setDy(int dy) {
        this.dy = dy;
    }

    public Rectangle getConteneur() {
        return conteneur;
    }

    public void setConteneur(Rectangle conteneur) throws Exception {
        if(conteneur!=null) this.conteneur = conteneur;
        else throw new Exception("No Container to add\n");
    }

    /*public Deplacable(int x, int y, int width, int height, Rectangle cont) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        conteneur=cont;
    }*/
    public Deplacable(Rectangle cont){
        conteneur=cont;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public abstract void deplacer();
}
