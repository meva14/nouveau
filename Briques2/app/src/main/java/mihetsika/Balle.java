package mihetsika;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;

import java.util.ArrayList;

import pannels.Rectangle;
import threads.DeplacerBall;

/**
 * Created by ITU on 04/04/2018.
 */
public class Balle extends Deplacable{

    private int indBalle;

    private boolean isLatsaka=false;
    private boolean mikasoka;
    private int dona;
    private Brique deux;
    private int vie;
    private DeplacerBall dep;


    public Brique getDeux(){
        return deux;
    }
    public void setDona(int n){
        dona=n;
    }
    public int getDona(){
        return dona;
    }
    public boolean getMikasoka(){
        return mikasoka;
    }
    public boolean getIsLatsaka(){
        return isLatsaka;
    }
    public void setIsLatsaka(boolean b){isLatsaka=b;}
    public void setMikasoka(Boolean b){
        mikasoka=b;
    }
    public Balle(Rectangle cont) {
        super(cont);
        width=30;
        height=30;
        dx=2;dy=-2;

        Plateau p=cont.getPlateau();
        int wp=p.getX();
        int hp=p.getY();

        x=wp+(p.getWidth()/2)-(width/2);
        y=hp-(width/2);
        setMikasoka(true);
    }
    public void setIndBalle(int i){
        indBalle=i;
    }
    public void afficher(Canvas canvas,Paint paint){
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.parseColor("#FFFFFF"));
        canvas.drawCircle(x,y,width/2,paint);
    }
    public void collisionPlateau(Plateau p){
        if(testSisiny(p)==true){
            setDerivee(-dx,-dy);
        }
        if(x>=p.getX() && x<=p.getWidth()+p.getX()){
            if((p.getY()-y)==(height/2)){
                setDy(-dy);
                mikasoka=true;
            }
        }
    }

    public void collisionLB(ArrayList<Brique> lb){

        for(int i=0;i<lb.size();i++){
            if(testerCollision((Brique)lb.get(i))==true){
                Brique br=(Brique)lb.get(i);
                if(dona==2){
                    deux=br;
                    vie=deux.getDona();
                    vie--;
                    deux.setDona(vie);
                }
                if(br==deux && dona>2){
                    vie--;
                    deux.setDona(vie);
                    Log.d("VIE APRES", String.valueOf(deux.getDona()));
                }
                if(deux!=null && deux.getDona()==0){
                    Log.d("VIE FIN", String.valueOf(deux.getDona()));
                    conteneur.getScore().setPoint(br.getPoint()*indBalle);
                    lb.remove(i);
                    break;
                }
                if(br!=deux) {
                    conteneur.getScore().setPoint(br.getPoint()*indBalle);
                    lb.remove(i);
                    break;
                }
            }
        }
    }
    public boolean testerCollision(Brique b){
        boolean test=false;
        if(testSisiny(b)==true){
            setDerivee(-dx,-dy);
            test=true;
        }
        if(x>=b.getX() && x<=b.getWidth()+b.getX()){
            if((Math.abs(b.getY() - y) <= (height / 2)) ||(Math.abs(y-b.getY())<=(b.getHeight()+(height/2)))){
                setDy(-dy);test=true;
            }
        }
        if(y>=b.getY() && y<=b.getY()+b.getHeight()){
            if(Math.abs(b.getX()-x)<=(width/2)){
                x=b.getX()-(width/2);setDx(-dx);test=true;
            }
            if(Math.abs(x-(b.getX()+b.getWidth()))<=(width/2)){
                x=b.getX()+b.getWidth()+(width/2);setDx(-dx);test=true;
            }
        }
        if(test){
            setDona(getDona()+1);
        }
        return test;
    }
    /*public boolean testSisinyPlateau(Plateau p){
        boolean test=false;
        int xGaucheHaut=p.getX();
        int yGaucheHaut=p.getY();
        int xDroiteHaut=p.getX()+p.getWidth();
        int yDroiteHaut=p.getY();
        if(Math.sqrt((xGaucheHaut-x)*(xGaucheHaut-x) + (yGaucheHaut-y)*(yGaucheHaut-y))<=(width/2)){
            test=true;
        }
        if(Math.sqrt((xDroiteHaut-x)*(xDroiteHaut-x) + (yDroiteHaut-y)*(yDroiteHaut-y))<=(width/2)){
            test=true;
        }
        return test;
    }*/
    public boolean testSisiny(Deplacable d){
        boolean test=false;
        int xGaucheHaut=d.getX();
        int yGaucheHaut=d.getY();
        int xDroiteHaut=d.getX()+d.getWidth();
        int yDroiteHaut=d.getY();
        int xGaucheBas=d.getX();
        int yGaucheBas=d.getY()+d.getHeight();
        int xDroiteBas=d.getX()+d.getWidth();
        int yDroiteBas=d.getY()+d.getHeight();
        if(Math.sqrt((xGaucheHaut-x)*(xGaucheHaut-x) + (yGaucheHaut-y)*(yGaucheHaut-y))<=(width/2)){
            test=true;
        }
        if(Math.sqrt((xGaucheBas-x)*(xGaucheBas-x) + (yGaucheBas-y)*(yGaucheBas-y))<=(width/2)){
            test=true;
        }
        if(Math.sqrt((xDroiteHaut-x)*(xDroiteHaut-x) + (yDroiteHaut-y)*(yDroiteHaut-y))<=(width/2)){
            test=true;
        }
        if(Math.sqrt((xDroiteBas-x)*(xDroiteBas-x) + (yDroiteBas-y)*(yDroiteBas-y))<=(width/2)){
            test=true;
        }
        return test;
    }
    public void setX(int coordx) {
        x = coordx;
        if(x<getWidth()/2){
            x=getWidth()/2;
            setDerivee(-getDx(),getDy());
        }
        if(x>conteneur.getWidth()-(getWidth()/2)){
            x=conteneur.getWidth()-(getWidth()/2);
            setDerivee(-getDx(),getDy());
        }
    }

    public void setY(int coordy) {
        y = coordy;
        if (y < (getHeight()/2)) {
            y = getHeight()/2;
            setDerivee(getDx(),-getDy());
        }
        if(y>conteneur.getHeight()-(getHeight()/2)){
            y=conteneur.getHeight()-(getHeight()/2);
            setDerivee(getDx(),-getDy());
        }
    }
    public void setDx(int nb){
        dx=nb;
    }
    public void setDy(int nb){
        dy=nb;
    }

    public int getNombre(){
        int val= (int) (Math.random()/0.1);
        return val;
    }

    @Override
    public void deplacer() {
        /*int nb=1;

        while(y<conteneur.getHeight()-(getHeight()/2)){
            if(nb%100000==0){
                setX(x+getDx());
                setY(y+getDy());

                mikasoka=false;
                collisionPlateau(conteneur.getPlateau());
                collisionLB(conteneur.getBriq());
                conteneur.postInvalidate();
            }
            nb++;
        }
        isLatsaka=true;*/
        dep=new DeplacerBall(this);
        dep.start();
    }

}
