package mihetsika;

import android.annotation.TargetApi;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;

import pannels.Rectangle;

/**
 * Created by ITU on 07/04/2018.
 */
public class Brique extends Deplacable {
    private int point;
    private int dona;

    public void setDona(int n){
        dona=n;
    }
    public int getDona(){
        return dona;
    }
    public int getPoint(){
        return point;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public Brique(Rectangle cont, int coordx, int coordy, int pt) {
        super(cont);
        width=(cont.getMinimumWidth()-50)/6;
        height=cont.getMinimumHeight()/20;
        x=coordx;
        y=coordy;
        point=pt;
        dona=3;
    }
    public void afficher(Canvas canvas, Paint paint){
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.parseColor("#FFAD036B"));
        canvas.drawRect(x,y,x+width,y+height,paint);
    }

    @Override
    public void deplacer() {

    }
}
