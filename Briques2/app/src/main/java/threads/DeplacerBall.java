package threads;

import mihetsika.Balle;

/**
 * Created by ITU on 04/05/2018.
 */
public class DeplacerBall extends Thread{
    private Balle ball;
    public DeplacerBall(Balle b){
        ball=b;
    }
    public void run(){
        int nb=1;
        while(ball.getY()<ball.getConteneur().getHeight()-(ball.getHeight()/2)){
            if(nb%100000==0){
                ball.setX(ball.getX()+ball.getDx());
                ball.setY(ball.getY()+ball.getDy());

                ball.setMikasoka(false);
                ball.collisionPlateau(ball.getConteneur().getPlateau());
                ball.collisionLB(ball.getConteneur().getBriq());
                ball.getConteneur().postInvalidate();
            }
            nb++;
        }
        ball.setIsLatsaka(true);
    }
}
