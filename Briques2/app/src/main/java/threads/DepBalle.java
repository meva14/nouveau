package threads;

import java.util.ArrayList;

import mihetsika.Balle;

/**
 * Created by ITU on 17/04/2018.
 */
public class DepBalle extends Thread {
    //private Balle ball;
    private int ind;
    private ArrayList<Balle> ball;
    public void setInd(int n){
        ind=n;
    }
    /*public DepBalle(Balle b){
        if(b!=null) {
            ball = b;
        }
    }*/
    public DepBalle(ArrayList<Balle> b){
        if(b!=null) {
            ball = b;
        }
    }
    /*public void run(){
        if(ball!=null){
            ball.deplacer();
        }
        if(ball.getIsLatsaka()==true){
            interrupt();
        }
    }*/
    public void run(){
        if(ball!=null){
            while(true){
                for (int i = 0; i < ball.size(); i++) {
                    ball.get(i).deplacer();
                    if(ball.get(i).getIsLatsaka()){
                        ball.remove(i);
                    }
                }
            }
        }
    }
}
