package services;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.example.itu.briques2.MainActivity;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpResponseException;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Created by ITU on 14/04/2018.
 */
public class MyAsyncTask extends AsyncTask<String,Object,String> {
    private static final String SOAP_ACTION="http://tempuri.org/Check";
    private static final String METHOD_NAME="Check";
    private static final String NAMESPACE="http://tempuri.org/";
    private String login;
    private String mdp;
    private Intent intent;
    private MainActivity mact;

    public MyAsyncTask(String lg,String pwd,Intent i,MainActivity ma){
        login=lg;
        mdp=pwd;
        intent=i;
        mact=ma;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if(s.compareTo("true")==0){
            intent.putExtra("log",login);
            mact.startActivity(intent);
        }
        if(s.compareTo("false")==0){
            mact.getSoratra().setText("Login introuvable\nVeuillez vous inscrire");
        }
        if(s.compareTo("true")!=0 && s.compareTo("false")!=0){
            mact.getSoratra().setText(s);
        }
    }

    @Override
    protected String doInBackground(String... params) {
        String URL="http://192.168.43.149:12/nouveau/Service1.asmx?op=Check";
        //String URL="http://192.168.43.149:12/EssaiWebServ/Service1.asmx?op=Check";
        SoapObject request=new SoapObject(NAMESPACE,METHOD_NAME);
        request.addProperty("log",login);
        request.addProperty("mdp",mdp);

        SoapSerializationEnvelope envelope=new SoapSerializationEnvelope(SoapEnvelope.VER11);
        //envelope.implicitTypes=true;
        envelope.setOutputSoapObject(request);
        envelope.dotNet =true;

        HttpTransportSE httpTransport=new HttpTransportSE(URL);
       // httpTransport.debug=true;

        SoapPrimitive soap=null;
        try {
            httpTransport.call(SOAP_ACTION, envelope);

        } catch(HttpResponseException e){
            Log.e("HTTPLOG",e.getMessage());
            e.printStackTrace();
        } catch(IOException e){
            Log.e("IOLOG",e.getMessage());
            e.printStackTrace();
        } catch(XmlPullParserException e){
            Log.e("XMLLOG",e.getMessage());
            e.printStackTrace();
        }

            try{
            soap=(SoapPrimitive) envelope.getResponse();
            Log.i("RESPONSE", String.valueOf(soap.toString()));
        } catch(SoapFault e){
                String err=e.getMessage().split("-->")[1];
               return err;
            }

        return soap.toString();
    }
}
