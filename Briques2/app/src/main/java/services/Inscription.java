package services;

import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.example.itu.briques2.Main4Activity;
import com.example.itu.briques2.MainActivity;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpResponseException;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Created by ITU on 18/04/2018.
 */
public class Inscription extends AsyncTask<String,Object,String> {
    private static final String SOAP_ACTION="http://tempuri.org/Inscription";
    private static final String METHOD_NAME="Inscription";
    private static final String NAMESPACE="http://tempuri.org/";
    private String login;
    private String mdp;
    private Intent intent;
    private Main4Activity mact;
    private String vola;

    public Inscription(String lg,String pwd,String d,Intent i,Main4Activity ma){
        login=lg;
        mdp=pwd;
        intent=i;
        vola=d;
        mact=ma;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if(s.compareTo("1")==0){
            mact.startActivity(intent);
        } else {
            mact.getError().setText(s);
        }
    }

    @Override
    protected String doInBackground(String... params) {
        String URL="http://192.168.43.149:12/nouveau/Service1.asmx?op=Inscription";
        //String URL="http://192.168.43.149:12/EssaiWebServ/Service1.asmx?op=Inscription";
        SoapObject request=new SoapObject(NAMESPACE,METHOD_NAME);
        request.addProperty("log",login);
        request.addProperty("mdp",mdp);
        request.addProperty("vola",vola);

        SoapSerializationEnvelope envelope=new SoapSerializationEnvelope(SoapEnvelope.VER11);
        //envelope.implicitTypes=true;
        envelope.setOutputSoapObject(request);
        envelope.dotNet =true;

        HttpTransportSE httpTransport=new HttpTransportSE(URL);
        // httpTransport.debug=true;

        try{
            httpTransport.call(SOAP_ACTION,envelope);

        } catch(HttpResponseException e){
            Log.e("HTTPLOG",e.getMessage());
            e.printStackTrace();
        } catch(IOException e){
            Log.e("IOLOG",e.getMessage());
            e.printStackTrace();
        } catch(XmlPullParserException e){
            Log.e("XMLLOG",e.getMessage());
            e.printStackTrace();
        }
        SoapPrimitive soap=null;
        try{
            soap=(SoapPrimitive) envelope.getResponse();
            Log.i("RESPONSE",String.valueOf(soap.toString()));
        } catch(SoapFault e){
           String err=e.getMessage().split("-->")[1];
            return err;
        }
        return soap.toString();
    }
}
