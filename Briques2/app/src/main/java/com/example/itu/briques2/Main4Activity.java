package com.example.itu.briques2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import services.Inscription;
import services.MyAsyncTask;

public class Main4Activity extends AppCompatActivity {

    private TextView error;

    public TextView getError(){
        return error;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);

        error=(TextView)findViewById(R.id.textView4);
        final Button sign=(Button)findViewById(R.id.button7);
        final EditText login=(EditText) findViewById(R.id.editText3);
        final EditText mdp=(EditText) findViewById(R.id.editText4);
        final EditText vola=(EditText) findViewById(R.id.editText5);

        if (sign != null) {
            sign.setOnClickListener(new View.OnClickListener(){

                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(Main4Activity.this,MainActivity.class);
                    if(login!=null && mdp!=null){
                        Inscription req=new Inscription(login.getText().toString(),mdp.getText().toString(),vola.getText().toString(),intent,Main4Activity.this);
                        req.execute();
                    }
                }
            });
        }
    }
}
