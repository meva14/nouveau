package com.example.itu.briques2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import listeners.ListenerBalle;
import listeners.ListenerDroite;
import listeners.ListenerGauche;
import listeners.ListenerNew;
import mihetsika.Plateau;
import pannels.Rectangle;
import pannels.TestView;
import threads.DepBalle;

public class Main3Activity extends AppCompatActivity {
    private RelativeLayout rl;
    private Rectangle rect;
    private DepBalle dep;
    private Button start;
    private Button b;
    private Button b1;
    private Button nouv;
    private int ind;

    public int getInd(){
        return ind;
    }
    public void setInd(int n){
        ind=n;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        rl=(RelativeLayout)findViewById(R.id.dessin);
        start=(Button) findViewById(R.id.button5);
        b=(Button) findViewById(R.id.button3);
        b1=(Button) findViewById(R.id.button4);
        nouv=(Button) findViewById(R.id.button9);
    }

    public void debut(){
        rect=new Rectangle(this);
        rl.addView(rect);
        //dep=new DepBalle(rect.getBalle());
        dep=new DepBalle(rect.getBalle());
        start.setOnClickListener(new ListenerBalle(dep));
        b.setOnClickListener(new ListenerGauche(rect.getPlateau()));
        b1.setOnClickListener(new ListenerDroite(rect.getPlateau()));
        nouv.setOnClickListener(new ListenerNew(rect));
    }

    protected void onStart(){
        super.onStart();
        debut();
    }
   protected void onPause(){
       super.onPause();
       try {
           dep.sleep(1000000);
       } catch (InterruptedException e) {
           e.printStackTrace();
       }
   }

}
