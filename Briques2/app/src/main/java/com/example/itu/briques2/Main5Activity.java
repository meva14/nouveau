package com.example.itu.briques2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Main5Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main5);

        Button b=(Button)findViewById(R.id.button8);
        if(b!=null){
            b.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    Intent in=new Intent(Main5Activity.this,Main3Activity.class);
                    startActivity(in);
                }
            });
        }
    }
}
