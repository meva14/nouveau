package com.example.itu.briques2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Intent intent=getIntent();
        TextView soratra=(TextView)findViewById(R.id.textView2);
        Button bouton=(Button) findViewById(R.id.button2);

        if(intent!=null){
            String ecriture="bienvenue ";
            ecriture=ecriture+intent.getStringExtra("log")+"!";
            soratra.setText(ecriture);
        }
        if(bouton!=null){
            bouton.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v) {
                Intent in = new Intent(Main2Activity.this, Main3Activity.class);
                startActivity(in);
                }
            });
        }

    }
}
