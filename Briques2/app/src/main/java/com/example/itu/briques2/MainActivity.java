package com.example.itu.briques2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import services.MyAsyncTask;

public class MainActivity extends AppCompatActivity {

    private TextView error;

    public TextView getSoratra(){
        return error;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button bouton = (Button) findViewById(R.id.button);
        final EditText login = (EditText) findViewById(R.id.editText);
        final EditText mdp = (EditText) findViewById(R.id.editText2);
        final Button insc = (Button) findViewById(R.id.button6);
        error = (TextView) findViewById(R.id.textView3);


        if (bouton != null) {
            bouton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                    if (login != null && mdp != null) {
                        MyAsyncTask req = new MyAsyncTask(login.getText().toString(), mdp.getText().toString(), intent, MainActivity.this);
                        req.execute();
                    }
                }
            });
        }
        if (insc != null) {
            insc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in = new Intent(MainActivity.this, Main4Activity.class);
                    startActivity(in);
                }
            });
        }

    }
}
