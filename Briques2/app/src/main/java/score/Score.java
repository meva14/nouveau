package score;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

/**
 * Created by ITU on 12/04/2018.
 */
public class Score {
    private int point;
    private String idUser;
    private int xCanvas;
    private int yCanvas;

    public void setSizeCanvas(int a,int b){
        xCanvas=a;
        yCanvas=b;
    }

    public Score(){}
    public void setPoint(int pt){
        point=point+pt;
    }

    public void afficher(Canvas canvas,Paint paint){
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.parseColor("#FFFFFF"));
        paint.setTextSize(30);
        canvas.drawText("SCORE:"+String.valueOf(point),xCanvas,yCanvas,paint);
        //canvas.drawText("SCORE:"+String.valueOf(point),300,600,paint);
    }
}
